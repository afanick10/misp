#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import click

import datetime
from dateutil import tz

import re
import zipfile

from pymisp import PyMISP

import csv
from docx import Document
from docx.shared import Pt, Inches
from docx.oxml.ns import nsdecls
from docx.oxml import parse_xml
from defang import defang

@click.command()
@click.option('--start', help='The start date for report in yyyy-mm-dd:hh format.')
@click.option('--end', help='The end date for report in yyyy-mm-dd:hh format.')
@click.option('--key', help="The user's API key.")
@click.option('--url', help='The url of the hostname to grab events from.')
@click.option('--name', help='The name for each file without an extension given by the user if they want to override the default file name.')
def clickHandler(start, end, key, url, name):
    handler(start, end, key, url, name)

def handler(start, end, key, url, name):
    
    if start == '' or start is None:
        print('You need to provide a start date.')
        exit(1)

    if end == '' or end is None:
        print('You need to provide an end date.')
        exit(1)

    try:
        start_date = datetime.datetime.strptime(start, '%Y-%m-%d:%H')
    except:
        print('Make sure the start date is in yyyy-mm-dd:hh format.')
        exit(1)

    try:
        end_date = datetime.datetime.strptime(end, '%Y-%m-%d:%H')
    except:
        print('Make sure the end date is in yyyy-mm-dd:hh format.')
        exit(1)

    if end_date < start_date:
        print('Your end date must be later than the start date.')
        exit(1)

    try:
        misp = PyMISP(url=url, key=key, ssl=False)
    except Exception as inst:
        print(inst.args[0])
        exit(1)

    result = misp.search(
        'events',
        published=True,
        publish_timestamp=start_date.date() - datetime.timedelta(days=1)
    )

    if len(result) == 0:
        print('Search did not turn up any MISP events.')
        exit(1)

    events = []

    for event in result:
        publish_timestamp = datetime.datetime.fromtimestamp(int(event['Event']['publish_timestamp']))
        publish_timestamp = publish_timestamp.replace(tzinfo=tz.gettz('UTC')).astimezone(tz.tzlocal())
        publish_timestamp = publish_timestamp.replace(minute=0, second=0, tzinfo=None)
        if start_date <= publish_timestamp <= end_date:
            events.append(event)

    if len(events) == 0:
        print('There are no events within the given timeframe.')
        exit(0)

    doc = Document()
    style = doc.styles['Normal']
    font = style.font
    font.name = 'Calibri (Body)'

    doc.add_paragraph()
    run = doc.paragraphs[0].add_run('Malware Indicators')
    run.font.bold = True
    run.font.size = Pt(16)

    doc.add_paragraph(
        '*IMPORTANT: These indicators must be silently dropped if implemented in defensive technologies.' +
        ' Many security products perform dynamic pulling of malicious sites/IPs. The indicators below are' +
        ' sensitive and may be associated with on-going investigations. Active probing disrupt such efforts.'
    )

    doc.add_paragraph(
        '\nNote: The indicators are available from CCCS\' MISP Logan Portal' +
        ' and may contain additional indicators, as some indicator sets can be quite large.\n'
    )

    for event in events:

        pre_infection_attributes, post_infection_attributes, pre_post_infection_attributes, infection_attributes = set_and_sort_lists(event)

        if not len(pre_infection_attributes) == len(post_infection_attributes) == len(pre_post_infection_attributes) == len(infection_attributes) == 0:

            table = doc.add_table(rows=1, cols=1, style='Table Grid')
            header_cells = table.rows[0].cells
            header_cells[0].text = 'Threat:\t' + event['Event']['info']
            header_cells[0].width = Inches(16)
            run = header_cells[0].paragraphs[0].runs[0]
            run.font.bold = True
            run.font.size = Pt(14)

            background_colour = parse_xml(r'<w:shd {} w:fill="D3D3D3"/>'.format(nsdecls('w')))
            header_cells[0]._tc.get_or_add_tcPr().append(background_colour)

            row_cells = table.add_row().cells
            background_colour_2 = parse_xml(r'<w:shd {} w:fill="F0F0F0"/>'.format(nsdecls('w')))
            row_cells[0]._tc.get_or_add_tcPr().append(background_colour_2)
            row_cells[0].width = Inches(16)
            row_cells[0].paragraphs[0].add_run('\n')

            lists_map = {
                'pre-infection': pre_infection_attributes, 
                'post-infection': post_infection_attributes, 
                'infection': infection_attributes, 
                'pre-post-infection': pre_post_infection_attributes
            }

            for key in lists_map:
                if key == 'pre-infection':
                    attribute_category = '\tPre-Infection\n\n'
                elif key == 'post-infection':
                    attribute_category = '\tPost-Infection\n\n'
                elif key == 'infection':
                    attribute_category = '\tInfection\n\n'
                else:
                    attribute_category = '\tPre and Post-Infection\n\n'

                if len(lists_map[key]) > 0:
                    row_cells[0].paragraphs[0].add_run(attribute_category).bold = True
                    add_category_to_document(lists_map[key], row_cells)

            row_cells = table.add_row().cells
            row_cells[0].width = Inches(16)

            reference_type = get_reference_type(event)
            row_cells[0].text += 'Reference:\t' + reference_type
            run = row_cells[0].paragraphs[0].runs[0]
            run.font.bold = True
            run.font.size = Pt(13)

            background_colour_3 = parse_xml(r'<w:shd {} w:fill="D3D3D3"/>'.format(nsdecls('w')))
            row_cells[0]._tc.get_or_add_tcPr().append(background_colour_3)

    if name:
        filename = name
    else:
        now = datetime.datetime.now().date()
        filename = now.strftime("%Y%m%d")

    doc.save(filename + '.TLP-amber.docx')

    field_names = ['IndicatorID',
                   'Indicator',
                   'indValue',
                   'Notes',
                   'InfectionType',
                   'IndicatorType',
                   'Malware',
                   'TLP',
                   'Reference',
                   'Confidence',
                   'ControlGroupID'
    ]

    csv_file = open(filename + '.TLP-amber.csv', 'w', newline='', encoding='utf-8')
    csv_writer = csv.DictWriter(csv_file, fieldnames=field_names)
    csv_writer.writeheader()

    for event in events:
        attributes = set_attributes_for_csv(event)

        ce_id = get_ce_id(event)
        control_document = get_control_document(event)

        if ce_id == '':
            indicator_id = control_document
        else:
            indicator_id = ce_id

        for attribute in attributes:
            indValue = ''
            if '|' in attribute['value']:
                indicator = attribute['value'][:attribute['value'].index('|')]
                indValue = attribute['value'][attribute['value'].index('|') + 1:]
            else:
                indicator = attribute['value']

            if is_event_tech_report_material(event) or is_ioc_harvesting(event):
                if (is_indicator_url(attribute['type'])):
                    indicator = defang(indicator, colon=True, all_dots=True)
                csv_writer.writerow({
                    'IndicatorID': indicator_id,
                    'Indicator': indicator,
                    'indValue': indValue,
                    'Notes': attribute['comment'],
                    'InfectionType': get_infection_type(attribute),
                    'IndicatorType': get_indicator_type(attribute),
                    'Malware': event['Event']['info'],
                    'TLP': get_tlp_level(event),
                    'Reference': get_reference_type(event),
                    'Confidence': get_confidence_level(event),
                    'ControlGroupID': event['Event']['id']
                })

    csv_file.close()

    zip_file = zipfile.ZipFile(filename + '.TLP-amber.zip', 'w')
    zip_file.write(filename + '.TLP-amber.docx')
    zip_file.write(filename + '.TLP-amber.csv')
    zip_file.close()

def add_category_to_document(list_of_attributes, row_cells):
    for i in range(len(list_of_attributes)):
        defangable = False
        if (is_indicator_url(list_of_attributes[i]['type'])):
            defangable = True
        if not (i > 0 and list_of_attributes[i]['type'] == list_of_attributes[i - 1]['type']):
            category = '\t\t' + list_of_attributes[i]['type'] + '\n\n'
            row_cells[0].paragraphs[0].add_run(category).bold = True
        if '|' in list_of_attributes[i]['value'] and 'filename' in list_of_attributes[i]['type']:
            row_cells[0].paragraphs[0].add_run('\t\t' + list_of_attributes[i]['value'][:list_of_attributes[i]['value'].find('|')] + '\n')
            total_runs = len(row_cells[0].paragraphs[0].runs)
            run = row_cells[0].paragraphs[0].runs[total_runs - 1]
            run.font.size = Pt(9)

            row_cells[0].paragraphs[0].add_run('\t\t' + list_of_attributes[i]['type'][list_of_attributes[i]['type'].find('|') + 1:].upper())
            total_runs = len(row_cells[0].paragraphs[0].runs)
            run = row_cells[0].paragraphs[0].runs[total_runs - 1]
            run.font.size = Pt(9)

            row_cells[0].paragraphs[0].add_run(' Hash: ' + list_of_attributes[i]['value'][list_of_attributes[i]['value'].find('|') + 1:] + '\n')
            total_runs = len(row_cells[0].paragraphs[0].runs)
            run = row_cells[0].paragraphs[0].runs[total_runs - 1]
            run.font.size = Pt(9)
        elif '|' in list_of_attributes[i]['value'] and 'ip' in list_of_attributes[i]['type']:
            value = list_of_attributes[i]['value'][:list_of_attributes[i]['value'].find('|')]
            if (defangable):
                value = defang(value, colon=True, all_dots=True)
            row_cells[0].paragraphs[0].add_run('\t\t' + value + '\n')
            total_runs = len(row_cells[0].paragraphs[0].runs)
            run = row_cells[0].paragraphs[0].runs[total_runs - 1]
            run.font.size = Pt(9)

            row_cells[0].paragraphs[0].add_run('\t\t' + list_of_attributes[i]['type'][list_of_attributes[i]['type'].find('|') + 1:].upper())
            total_runs = len(row_cells[0].paragraphs[0].runs)
            run = row_cells[0].paragraphs[0].runs[total_runs - 1]
            run.font.size = Pt(9)

            row_cells[0].paragraphs[0].add_run(': ' + list_of_attributes[i]['value'][list_of_attributes[i]['value'].find('|') + 1:] + '\n')
            total_runs = len(row_cells[0].paragraphs[0].runs)
            run = row_cells[0].paragraphs[0].runs[total_runs - 1]
            run.font.size = Pt(9)
        else:
            value = list_of_attributes[i]['value']
            if (defangable):
                value = defang(value, colon=True, all_dots=True)
            string_to_add = '\t\t' + value + '\n'
            if len(string_to_add) > 93:
                split_string = []

                for j in range(0, len(string_to_add), 50):
                    split_string.append(string_to_add[j:j + 50])

                for part in split_string:
                    if not '\t\t' in part:
                        if not '\n' in part:
                            row_cells[0].paragraphs[0].add_run('\t\t' + part + '\n')
                            total_runs = len(row_cells[0].paragraphs[0].runs)
                            run = row_cells[0].paragraphs[0].runs[total_runs - 1]
                            run.font.size = Pt(9)
                        else:
                            row_cells[0].paragraphs[0].add_run('\t\t' + part)
                            total_runs = len(row_cells[0].paragraphs[0].runs)
                            run = row_cells[0].paragraphs[0].runs[total_runs - 1]
                            run.font.size = Pt(9)
                    else:
                        row_cells[0].paragraphs[0].add_run(part + '\n')
                        total_runs = len(row_cells[0].paragraphs[0].runs)
                        run = row_cells[0].paragraphs[0].runs[total_runs - 1]
                        run.font.size = Pt(9)
            else:
                row_cells[0].paragraphs[0].add_run(string_to_add)
                total_runs = len(row_cells[0].paragraphs[0].runs)
                run = row_cells[0].paragraphs[0].runs[total_runs - 1]
                run.font.size = Pt(9)

        row_cells[0].paragraphs[0].add_run('\t\tNote: ' + list_of_attributes[i]['comment'] + '\n\n')
        total_runs = len(row_cells[0].paragraphs[0].runs)
        run = row_cells[0].paragraphs[0].runs[total_runs - 1]
        run.font.size = Pt(9)

def is_indicator_url(indicatorType):
    return indicatorType in ['domain', 'domain|ip', 'hostname', 'hostname|port', 'uri', 'url', 'ip-dst', 'ip-dst|port', 'ip-src', 'ip-src|port']

def get_indicator_type(attribute):
    domain = ['domain', 'domain|ip', 'hostname', 'hostname|port']
    email = ['email-attachment', 'email-body', 'email-body', 'email-dst', 'email-dst-display-name', 'email-message-id', 'email-src', 'email-src-display-name', 'email-subject']
    file = ['filename', 'filename|imphash', 'filename|md5', 'filename|sha1', 'filename|sha256', 'filename|sha512', 'filename|ssdeep', 'imphash', 'md5', 'sha1', 'sha256', 'sha512', 'ssdeep', 'pdb']
    ip = ['ip-dst', 'ip-dst|port', 'ip-src', 'ip-src|port']
    http_uri = ['uri']
    mutex = ['mutex']
    registry = ['regkey', 'regkey|value']
    url = ['url']
    user_agent = ['user-agent']

    if attribute['type'] in email:
        return 'Email Indicator(s)'
    elif attribute['type'] in file:
        return 'File Indicator(s)'
    elif attribute['type'] in url:
        return 'URL Indicator(s)'
    elif attribute['type'] in domain:
        return 'Domain Indicator(s)'
    elif attribute['type'] in http_uri:
        return 'HTTP URI Indicator(s)'
    elif attribute['type'] in mutex:
        return 'Mutex Indicator(s)'
    elif attribute['type'] in ip:
        return 'IP Indicator(s)'
    elif attribute['type'] in registry:
        return 'Registry Indicator(s)'
    elif attribute['type'] in user_agent:
        return 'User-Agent Indicator(s)'
    else:
        return 'String Indicator(s)'

def get_ce_id(event):
    ce_ids = []
    ce = re.compile(r'CE20[0-9]{2}[\d\-]+')
    be = re.compile(r'BE20[0-9]{2}[\d\-]+')
    incident = re.compile(r'INCIDENT\-[0-9]*')

    for attribute in event['Event']['Attribute']:
        if attribute['category'] == 'Internal reference' and attribute['type'] == 'text' and \
                (re.match(ce, attribute['value']) or re.match(be, attribute['value'])):
            ce_ids.append(attribute['value'] + '-' + event['Event']['id'])

    if len(ce_ids) > 1:
        if all(re.match(incident, ce_id) for ce_id in ce_ids):
            return ' | '.join(ce_ids)
        else:
            print('Found more than one Internal reference of type text matching CE__-% SQL LIKE statement associated with this event.')
            exit(1)

    if len(ce_ids) > 0:
        return ce_ids[0]
    else:
        return ''

def get_control_document(event):
    control_documents = ''
    if event['Event']['Orgc']['name'] == 'CSE-CST':
        return 'CSE Event'

    documents_to_add = []

    for attribute in event['Event']['Attribute']:
        if attribute['category'] == 'Internal reference' and attribute['type'] == 'text':
            documents_to_add.append(attribute['value'])

    if len(documents_to_add) == 0:
        print('Could not find an Internal text reference for control documents associated with this event')
        exit(1)

    for i in range(len(documents_to_add)):
        if i > 1:
            control_documents += ' | ' + documents_to_add[i].strip()
        else:
            control_documents = documents_to_add[i]

    return control_documents + '-' + event['Event']['id']

def is_event_tech_report_material(event):
    for tag in event['Event']['Tag']:
        if tag['name'] == 'ccirc:report="technical"' or 'cccs:report="technical"':
            return True

    return False

def is_ioc_harvesting(event):
    for tag in event['Event']['Tag']:
        if tag['name'] == 'ccirc:report="ioc-harvesting"' or 'cccs:report="ioc-harvesting"':
            return True

    return False

def get_infection_type(attribute):
    pre_infection_categories = ['Targeting data', 'Payload delivery']
    post_infection_categories = ['Persistence mechanism', 'Network activity', 'Artifacts dropped',
                                 'Payload installation']
    infection_categories = ['Other', 'Antivirus detection']

    if attribute['category'] in pre_infection_categories:
        return 'Pre-Infection'
    elif attribute['category'] in post_infection_categories:
        return 'Post-Infection'
    elif attribute['category'] in infection_categories:
        return 'Infection'
    else:
        return 'Pre and Post-Infection'

def get_reference_type(event):
    for tag in event['Event']['Tag']:
        if 'cccs:source' in tag['name']:
            if tag['name'] == 'cccs:source="cccs-research"' or tag['name'] == 'cccs:source="internal"':
                return 'CCCS Research'
            elif tag['name'] == 'cccs:source="trusted-partner"' or tag['name'] == 'ccirc:source="trusted-partner"':
                return 'Trusted Partner'
            elif tag['name'] == 'ccirc:source="ccirc-research"' or tag['name'] == 'ccirc:source="internal"':
                return 'CCIRC Research'
            elif tag['name'] == 'cccs:source="open-source"' or tag['name'] == 'ccirc:source="open-source"':
                return 'Open Source'
            elif tag['name'] == 'cccs:source="international-partner"' or tag['name'] == 'ccirc:source="international-partner"':
                return 'International Partner'
            elif tag['name'] == 'cccs:source="federal-partner"':
                return 'Federal Partner'
            elif tag['name'] == 'ccirc:source="sector-partner"':
                return 'Sector Partner'
            elif tag['name'] == 'ccirc:source="federal"':
                return 'Federal'
            else:
                return 'Trusted Source'

    return ''

def set_attributes_for_csv(event):
    attributes = []

    for attribute in event['Event']['Attribute']:
        if attribute['type'] == 'link' or attribute['type'] == 'filename|ssdeep' or attribute['type'] == 'ssdeep' or \
                attribute['type'] == 'email-body' or attribute['type'] == 'text' or attribute['type'] == 'yara' or \
                attribute['type'] == 'windows-scheduled-task' or attribute['category'] == 'External analysis':
            continue

        attributes.append(attribute)

    for object in event['Event']['Object']:
        for attribute in object['Attribute']:
            if attribute['type'] == 'link' or attribute['type'] == 'filename|ssdeep' or attribute['type'] == 'ssdeep' or \
                    attribute['type'] == 'email-body' or attribute['type'] == 'text' or attribute['type'] == 'yara' or \
                    attribute['type'] == 'datetime' or attribute['type'] == 'port' or attribute['type'] == 'named pipe':
                continue

            attributes.append(attribute)

    return attributes

def get_tlp_level(event):
    for tag in event['Event']['Tag']:
        if 'tlp' in tag['name']:
            tlp = tag['name'][:3].upper()
            colour = tag['name'][4:]
            return tlp + '-' + colour

    return ''

def get_confidence_level(event):
    for tag in event['Event']['Tag']:
        if 'confidence' in tag['name']:
            equal_sign_index = tag['name'].index('=')
            confidence = tag['name'][equal_sign_index + 1:]
            confidence = confidence[1:-1]
            return confidence.upper()

    return ''

def set_and_sort_lists(event):
    pre_infection_categories = ['Targeting data', 'Payload delivery']
    post_infection_categories = ['Persistence mechanism', 'Network activity', 'Artifacts dropped', 'Payload installation']
    infection_categories = ['Other', 'Antivirus detection']

    pre_infection_attributes = []
    post_infection_attributes = []
    pre_post_infection_attributes = []
    infection_attributes = []

    for attribute in event['Event']['Attribute']:
        if attribute['type'] == 'link' or attribute['type'] == 'filename|ssdeep' or \
                attribute['type'] == 'ssdeep' or attribute['type'] == 'email-body' or \
                attribute['type'] == 'text' or attribute['type'] == 'yara':
            continue
        if attribute['category'] in post_infection_categories:
            post_infection_attributes.append(attribute)
        elif attribute['category'] in pre_infection_categories:
            pre_infection_attributes.append(attribute)
        elif attribute['category'] in infection_categories:
            infection_attributes.append(attribute)
        else:
            if attribute['category'] != 'Internal reference':
                pre_post_infection_attributes.append(attribute)

    pre_infection_attributes = sorted(pre_infection_attributes, key=lambda attribute: attribute['type'])
    post_infection_attributes = sorted(post_infection_attributes, key=lambda attribute: attribute['type'])
    pre_post_infection_attributes = sorted(pre_post_infection_attributes, key=lambda attribute: attribute['type'])
    infection_attributes = sorted(infection_attributes, key=lambda attribute: attribute['type'])

    return pre_infection_attributes, post_infection_attributes, pre_post_infection_attributes, infection_attributes


if __name__ == '__main__':
    clickHandler()

