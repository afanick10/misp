# Weekly Technical IOC Report Generator

Command Line Interface (CLI) that outputs a zip file containing a .csv file and a .docx file containing information on MISP events within a date range. The .docx file contains a report for each MISP event while the .csv file contains similar information in a different format. The MISP events are taken from the MISP API (https://api.logan.ops.cyber.gc.ca).

The MISP server for this API is accessible here: https://logan.ccirc-ccric.gc.ca/

To obtain your MISP API key, read the instructions in the MISP API documentation in the following link:
https://www.circl.lu/doc/misp/automation/index.html#automation-key


## Installation Instructions 

### Binary Release
Visit the Release page of the project https://gitlab.acid.azure.chi/ino/ioc-weekly-report-cli/-/releases
And download the .exe command line tool. This tool will run in etiher powershell or git for Windows / git bash on the Falcon laptops.

You can also download the binary directly [here](https://gitlab.acid.azure.chi/ino/ioc-weekly-report-cli/-/raw/master/dist/main.exe)

### Manual install
```
git clone git@gitlab.acid.azure.chi:ino/ioc-weekly-report-cli.git
cd ioc-weekly-report-cli
python3 -m venv weekly-env
source weekly-env/bin/activate
pip install -r requirements.txt
```

### Building a new binary release

```
cd ioc-weekly-report-cli
source weekly-env/bin/activate
pip install pyinstaller
pyinstaller weekly/main.spec
```

the new .exe should be in the `dist/` folder.

## Usage Example
To run the CLI, input the following command:

`./main.exe --start YYYY-MM-DD:HH --end YYYY-MM-DD:HH --key API_KEY --url HOST_URL`

Optionally, the user can add the following argument:

`--name FILE_NAME_WITHOUT_EXTENSION`

## Test Case
To test the application, follow these steps:
1. Copy the .exe to your Documents in Windows
2. Open powershell from the start menu
3. Type in `cd $home/Documents` and press enter
4. Type in `./main.exe --start 2021-03-24:10 --end 2021-03-31:10 --key *Insert your MISP API key* --url https://api.logan.ops.cyber.gc.ca --name 20210331` and press enter

HTTPS warnings are normal and a new version of the project will fix that in the future.
- In your Documents folder you should now see:
  - One file called `20210331.TLP-amber.csv` that contains *1429* lines. All values in the `Malware` column should prefixed by one of the following list: `TA21-0111`, `TA21-0113`, `TA21-0115`, `TA21-0116`, `TA21-0119`, `TA21-0105`.
  - One, 6 pages long, document called `20210331.TLP-amber.docx` that contains a table with the title starting with `TA21-0105 `.

  The DOCX is a summary of the CSV and *CAN* contain less information.


### Validating Reports

After generating the reports, here are *some* steps to validate the output.

To help, start by login to https://logan.ccirc-ccric.gc.ca/
You can filter down the events on the website by using the magnifying glass button. This allows to only see events included in the report's dates.

Example of validation steps:
- Does the list on the website and the csv have the same TA ids? (Ex: `TA21-0105`)?
  - are any missing? Why?
  - are any extraneous? Why?
  - Does the CSV events and the Docx events match? I.e does the docx events all contained in the CSV?
- Is the classification of the events match the website and are the files correctly classified? (Ex: no TLP:RED in the TLP:Amber file)
- Are the attributes structurally good? (Example: Are there extra white space around urls?)
- Are the report dates the correct ones in the right year/month?

## HYDRA
To get the events when running the package in HYDRA, provide the following url as a command line argument:

https://logan.ccirc-ccric.gc.ca:8443


### Requesting an Account on Logan
The Logan Misp server uses the Hydra Enclave credentials.
To request a Logan account send an email to *innovation@cyber.gc.ca* and *cybertech@cyber.gc.ca*.
One your Hydra account is setup with cybertech, INO *or any other admin on the Logan Misp server* can give your account permissions by creating an account with the same email on the Misp server.
