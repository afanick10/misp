#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup

dependencies = open('requirements.txt', 'r').read().split('\n')

setup(
    name='weekly',
    version='0.1.0',
    packages=['weekly'],
    url='https://gitlab.acid.azure.chi/ino/ioc-weekly-report-cli/',
    license='',
    author='Aaron Fanick',
    author_email='Daniel.Fanick@cyber.gc.ca',
    #team_email='innovation@cyber.gc.ca',
    description='Command Line Interface tool for generating reports on MISP events.',
    install_requires=dependencies,
    entry_points={'console_scripts': ['weekly=weekly.main:handler']}
)

